var createError = require('http-errors');
var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');

var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var configDB = require('./config/database.js');

var app = express();

app.set('view engine', 'ejs');
app.use(cookieParser());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(express.static('public'));

mongoose.connect(configDB.url); // connect to our database

var morgan = require('morgan');
app.use(morgan('dev'));

require('./config/passport')(passport); // init passport
app.use(session({
  secret: '197d120559222e44bf5b544a6aa0713468eb7f10daeeea3b0f31001af3c11c13fb560b9bf82c54484bfd717080ce078b8c0dbc25c2a92ef065c755da9f4a266a',
  name: 'login',
  resave: true,
  saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

// routes
require('./app/routes/index.js')(app);
require('./app/routes/account.js')(app);
require('./app/routes/admin.js')(app);
require('./app/routes/login.js')(app, passport);
require('./app/routes/mail.js')(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
console.log('Running on *3361');
