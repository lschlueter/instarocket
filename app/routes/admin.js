var moment = require('moment');
var User = require('../../app/models/user');
var MailController = require('../../app/controllers/mailController');

module.exports = function (app) {

    // admin
    app.get('/admin', isAdmin, function (req, res) {
        User.find({}, function (err, users) {
            if (err) return done(err);
            if (users) {
                res.render('admin/admin.ejs', {
                    user: req.user,
                    users: users,
                    loggedIn: req.isAuthenticated(),
                    moment: moment
                });
            }
        });
    });

    // view user
    app.get('/admin/user/:id', isAdmin, function (req, res) {
        process.nextTick(function () {
            User.find({_id: req.params.id}, function (err, targetUser) {
                if (err) return;
                res.render('admin/user.ejs', {
                    user: req.user,
                    loggedIn: req.isAuthenticated(),
                    moment: moment,
                    targetUser: targetUser
                });
            });
        });
    });

    // remove user
    app.get('/admin/user/remove/:id', isAdmin, function (req, res) {
        if (req.user._id === req.params.id) {
            res.redirect('/admin');
            return;
        }
        process.nextTick(function () {

            User.remove({
                _id: req.params.id
            }, function (err) {
                if (err) throw err;
                res.redirect('/admin');
            });
        });
    });

    // add user
    app.post('/admin/user/add', isAdmin, function (req, res) {
        User.findOne({email: req.body.email})
            .exec()
            .then(function (user) {
                if (user) return;

                const newUser = new User();
                newUser.firstName = req.body.firstName;
                newUser.name = req.body.name;
                newUser.lastIp = (req.headers['x-forwarded-for'] || '').split(',')[0] || req.connection.remoteAddress;
                newUser.lastLogin = Date.now();
                newUser.admin = false;
                newUser.botType = req.body.botType;
                newUser.botState = 'OFFLINE';
                newUser.botId = null;

                // set the user's local credentials
                newUser.local.email = req.body.email;
                newUser.local.password = newUser.generateHash(req.body.password);

                // set user's instagram daten
                newUser.instagram.username = null;
                newUser.instagram.password = null;

                newUser.settings.hashtags = [];
                newUser.settings.competitor_users = [];
                newUser.settings.mode = null;

                newUser.save(function (err) {
                    if (err) {
                        throw err;
                    } else {
                        MailController.sendMail(
                            newUser.local.email,
                            'Deine Zugansdaten',
                            'Hallo ' + newUser.firstName + ','
                            + '<br/>' + '<br/>' +
                            'vorab bedanken wir uns bei dir, dass du dich für das ' + req.body.botType + '-Paket entschieden hast!' +
                            '<br/>' + '<br/>' +
                            'Hier sind deine Zugangsdaten, mit denen du dich auf https://insta-rocket.de/login anmelden kannst.'
                            + '<br/>' + '<br/>' + 'E-Mail: <strong>' + newUser.local.email + '</strong>' + '<br/>' +
                            'Passwort: <strong>' + req.body.password + '</strong>' + '<br/>' + '<br/>' +
                            'Sobald du dich angemeldet hast, solltest du dein Passwort ändern und anfangen deine Hashtags und Zielaccounts zu definieren. Das alles passiert im Bereich "Profil". '
                            + '<br/>' + '<br/>' +
                            'Falls du Probleme hast oder dir Ideen zur Verbesserung einfallen, kannst du gerne eine E-Mail an <strong>instarocket@sublime-media.com</strong> schreiben!'
                            + '<br/>' + '<br/>' + '<br/>' +
                            'Danke und viele Grüße,'
                            + '<br/>' +
                            'Dein InstaRocket Team'
                        );
                        res.redirect('/admin');
                    }
                });
            })
            .catch(function (error) {
                if (error) console.log(error);
            });
    });

    app.get('/admin/user/run/:id', isAdmin, function (req, res) {
        const BotController = require('../../app/controllers/botController');
        User.findById(req.params.id, function (err, user) {
            if(err) console.log(err);
            if(user.botId) {
                BotController.stopProccess(user);
            } else {
                BotController.startProcess(user);
            }
        });
        res.redirect('/admin');
    });

    function isAdmin(req, res, next) {
        if (req.isAuthenticated() && req.user.admin) return next();
        res.redirect('/');
    }
};