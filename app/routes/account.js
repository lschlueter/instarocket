var moment = require('moment');

module.exports = function (app) {

    // account
    app.get('/account', isLoggedIn, function (req, res) {
        res.render('account.ejs', {
            moment: moment,
            user: req.user,
            loggedIn: req.isAuthenticated(),
            query: req.query,
            info: req.flash('info'),
            error: req.flash('error')
        });
    });

    // change password
    app.get('/account/changepw', isLoggedIn, function (req, res) {
        res.render('changepw.ejs', {message: ''});
    });

    app.post('/account/changepw', isLoggedIn, function (req, res) {
        const user = req.user;
        const oldPassword = req.body.oldPassword, newPassword = req.body.newPassword;

        if (!user.validPassword(oldPassword)) {
            res.render('changepw.ejs', {message: 'Dein Passwort ist nicht richtig.'});
            return;
        }

        if (oldPassword === newPassword) {
            res.render('changepw.ejs', {message: 'Dein neues Passwort muss sich dem aktuellen unterscheiden.'});
            return;
        }

        user.local.password = user.generateHash(newPassword);
        user.save(function (err) {
            if (err) {
                req.flash('error', 'Fehler beim ausführen. Bitte an Admin wenden.');
            }
            req.flash('info', 'Passwort wurde geändert.');
            res.redirect('/account');
        });
    });


    // edit user
    app.post('/account/user/edit', isLoggedIn, function (req, res) {
        const user = req.user;

        if (req.body.username && req.body.password) {
            user.instagram.username = req.body.username;
            user.instagram.password = req.body.password;
        }

        if (req.body.hashtags && req.body.competitor_users) {
            const hashtags = req.body.hashtags.filter(function (e) {
                return e;
            });
            const competitor_users = req.body.competitor_users.filter(function (e) {
                return e;
            });

            user.settings.hashtags = hashtags;
            user.settings.competitor_users = competitor_users;
        }

        user.save(function (err) {
            if (err) {
                req.flash('error', 'Fehler beim ausführen. Bitte an Admin wenden.');
            }
            req.flash('info', 'Daten wurden geändert.');
            res.redirect('/account');
        });
    });
};

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next();
    res.redirect('/');
}