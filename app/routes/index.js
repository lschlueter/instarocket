module.exports = function (app) {

    // index
    app.get('/', function (req, res) {
        res.render('index.ejs', {user: req.user, loggedIn: req.isAuthenticated()});
    });

    // imprint
    app.get('/imprint', function (req, res) {
        res.render('imprint.ejs', {user: req.user, loggedIn: req.isAuthenticated()});
    });

    // thank you
    app.get('/thankyou', function (req, res) {
        res.render('thankyou.ejs', {user: req.user, loggedIn: req.isAuthenticated()});
    });
};