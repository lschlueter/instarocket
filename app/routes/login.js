module.exports = function (app, passport) {

    // login
    app.get('/login', function (req, res) {
        res.render('login.ejs', {message: req.flash('loginMessage')});
    });

    // login post
    app.post('/login', passport.authenticate('local-login', {
        successRedirect: '/account',
        failureRedirect: '/login',
        failureFlash: true
    }));

    // signup
    app.get('/signup', function (req, res) {
        res.render('signup.ejs', {message: req.flash('signupMessage')});
    });

    // signup post
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect: '/account',
        failureRedirect: '/signup',
        failureFlash: true
    }));

    // logout
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/');
    });
};
