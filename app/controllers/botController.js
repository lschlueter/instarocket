const Bot = require('../models/bot');
const User = require('../models/user');
const Process = require("instagrambotlib");

module.exports = {

    userValid: function (user) {
        user = new User(user);
        if (typeof user.instagram.username == 'undefined') {
            console.log("Instagram username undefined");
            return false;
        }
        if (typeof user.instagram.password == 'undefined') {
            console.log("Insta PW undefined");
            return false;
        }
        if (user.settings.hashtags === '[]') {
            console.log("Hashtags undefined");
            return false;
        }
        if (user.settings.competitor_users === '[]') {
            console.log("Competitor Users undefined");
            return false;
        }
        return true;
    },

    makeBotEntry: function (user) {
        const bot = new Bot();
        bot.userId = user.id;
        user.botId = bot.id;

        bot.save();

        user.bot = bot;
        user.save();

        module.exports.updateUserState(user, 'STARTING..');
        return bot;
    },

    startAll: function () {
        User.find({}, function (err, users) {
                users.forEach(function (user) {
                    module.exports.init(user);
                });
            }
        )
    },

    startProcess: function (user) {
        if (user && !user.botId) {
            const bot = module.exports.makeBotEntry(user);
            if (!bot) return;

            const config = module.exports.createConfig(user);
            new Process(config).start();
        }
    },

    stopProccess: function (user) {
        if (user && user.botId) {
            const bot = module.exports.makeBotEntry(user);
            if (!bot) return;

            const config = module.exports.createConfig(user);
            new Process(config).stop();
        }
    },

    deleteBotEntry: function (user) {
        const bot = user.bot;
        bot.remove();
        user.bot = null;
        user.botId = null;
        user.botState = 'OFFLINE';
        user.save();
    },

    updateUserState: function (user, state) {
        user.botState = state;
        user.save();
    },

    createConfig: function (user) {
        const config = require("../../config/bot");
        config.instagram_username = user.instagram.username;
        config.instagram_password = user.instagram.password;
        config.instagram_hashtag = user.settings.hashtags;

        likemode_competitor_users = [];

        for (var i = 0; i < user.settings.competitor_users.length; i++) {
            var competitor_user = user.settings.competitor_users[i];
            likemode_competitor_users.push({'account': competitor_user});
        }

        config.likemode_competitor_users = likemode_competitor_users;
        return config;
    },
};