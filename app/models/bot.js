var mongoose = require('mongoose');

var botSchema = mongoose.Schema({

    userId: String,

    // meta
    created_at: Date,
    updated_at: Date
});

var Bot = mongoose.model('Bot', botSchema);
module.exports = Bot;