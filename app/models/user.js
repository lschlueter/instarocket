var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

var userSchema = mongoose.Schema({

    // user information
    firstName: String,
    name: String,
    lastIp: String,
    lastLogin: Date,
    admin: Boolean,

    // login
    local: {
        email: String,
        password: String
    },

    // instagram
    instagram: {
        username: String,
        password: String
    },

    botId: String,
    botType: String,
    botState: String,

    // settings
    settings: {
        hashtags: [],
        competitor_users: [],
        mode: String
    },

    // meta
    created_at: Date,
    updated_at: Date
});

userSchema.pre('save', function(next){
    var user = this;

    //check if password is modified, else no need to do anything
    if (!user.isModified('password')) {
        return next();
    }

    user.password = this.generateHash(user.password);
    next();
});

userSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.local.password);
};

var User = mongoose.model('User', userSchema);
module.exports = User;